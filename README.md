# Ikxbot core

## Access levels
| Level    | Description  | Extra information                                  |
|----------|--------------|----------------------------------------------------|
| 0        | Guest        | User has not been logged in                        |
| 10       | Voice        | User has voice access on channel                   |
| 11       | Known user   | User has been logged in to his/her account         |
| 20       | Halfop       | User has halfop modes on channel                   |
| 30       | Op           | User has op privileges on channel                  |
| 40       | Admin        | User has admin privileges on channel               |
| 50       | Owner        | User has owner privileges on channel               |
| 60       | IRCop        | User is an IRC op on the network                   |
| 1000     | Bot owner    | Highest possible level, all commands are available |

## Commands available
| Command      | Minimum access level | Description |
|--------------|----------------------|-------------|
| !Diagnose    | 1                    | Show information about the bot and the system running the bot |
| !Help        | 0                    | Show a help with all available commands |
| !Ping        | 0                    | Check if the bot is still alive |
| !Setprefix   | 100                  | Change the bot's command prefix |
| !Whoami      | 0                    | Show information about the current user |
| !Whois       | 0                    | Show information about the given user |
In this table, **!** is the command prefix. You can change the command
prefix in your configuration.