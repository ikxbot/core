<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core;

use Ikx\Core\Model\Config;

/**
 * Ikx's core application class
 * @package Ikx\Core
 */
class Application extends \Symfony\Component\Console\Application {
    const VERSION = '1.1.3';

    /**
     * @var Config Configuration holder
     */
    private static $config;
    private static $locale = 'en_US';

    /**
     * Application constructor.
     * @param string $name
     * @param string $version
     */
    public function __construct($name = 'UNKNOWN', $version = 'UNKNOWN')
    {
        parent::__construct("Ikx IRC Bot", self::VERSION);
    }

    /**
     * Fetch and/or load the configuration
     * @return Config
     */
    public static function config() {
        if (!self::$config) {
            // self::$config = Config::load(IKX_ROOT . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'config.yml');
            self::$config = \Noodlehaus\Config::load(IKX_ROOT . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'config.yml');
        }

        return self::$config;
    }

    public static function getLocale() {
        return self::$locale;
    }

    public static function setLocale($locale) {
        self::$locale = $locale;
    }
}