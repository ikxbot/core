<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Utils;

/**
 * Formatting utility
 * @package Ikx\Core\Utils
 */
class Format {
    /**
     * Colorize the text
     * @param string $text
     * @param int $foreground
     * @param int|null $background
     * @return string
     */
    public static function color($text, $foreground, $background = null) {
        if ($foreground < 10) { $foreground = '0' . $foreground; }
        if (!is_null($background) && $background < 10) { $background = '0' . $background; }

        $color = $foreground;

        if (!is_null($background)) {
            $color .= ',' . $background;
        }

        return sprintf("\x03%s%s\x03", $color, $text);
    }

    /**
     * Underline the text
     * @param string $text
     * @return string
     */
    public static function underline($text) {
        return sprintf(chr(31) . "%s" . chr(31), $text);
    }

    /**
     * Make the text bold
     * @param string $text
     * @return string
     */
    public static function bold($text) {
        return sprintf("\x02%s\x02", $text);
    }

    /**
     * Make the text italic
     * @param string $text
     * @return string
     */
    public static function italic($text) {
        return sprintf(chr(29) . "%s" . chr(29), $text);
    }

    /**
     * Reverse text colors
     * @param string $text
     * @return string
     */
    public static function reverse($text) {
        return sprintf(chr(22) . "%s" . chr(22), $text);
    }

    public static function strip($text) {
        $text = str_replace(chr(22), "", $text);
        $text = str_replace(chr(29), "", $text);
        $text = str_replace("\x02", "", $text);
        $text = str_replace(chr(31), "", $text);
        $text = str_replace("\x15", "", $text);
        $text = preg_replace("/\x03([0-9]+)\,?([0-9]+)?/i", "", $text);
        $text = str_replace("\x03", "", $text);

        return $text;
    }
}