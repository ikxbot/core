<?php
namespace Ikx\Core\Utils;

trait MessagingTrait {

    /**
     * Send a (private) message to a user or channel
     * @param string $target
     * @param string $data
     */
    public function msg($target, $data) {
        $data = wordwrap($data, 460, "\n", false);
        $ex = explode("\n", $data);
        foreach($ex as $ln) {
            $this->server->write(sprintf("PRIVMSG %s :%s", $target, $ln));
        }
    }

    /**
     * Send a notice to a user or channel
     * @param string $target
     * @param string $data
     */
    public function notice($target, $data) {
        $data = wordwrap($data, 460, "\n", false);
        $ex = explode("\n", $data);
        foreach($ex as $ln) {
            $this->server->write(sprintf("NOTICE %s :%s", $target, $ln));
        }
    }

    /**
     * Send an action message (/me in mIRC)
     * @param string $target
     * @param string $data
     */
    public function action($target, $data) {
        $this->msg($target, chr(1) . "ACTION " . $data . chr(1));
    }

    /**
     * Send an CTCP message
     * @param string $target
     * @param string $type
     * @param string|null $data
     */
    public function ctcp($target, $type, $data = null) {
        $this->msg($target, chr(1) . $type . ' ' . $data . chr(1));
    }

    /**
     * Send an CTCP reply
     * @param string $target
     * @param string $type
     * @param string|null $data
     */
    public function ctcpreply($target, $type, $data) {
        $this->notice($target, chr(1) . $type . ' ' . $data . chr(1));
    }
}