<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Utils;

/**
 * Table generating utility
 * @package Ikx\Core\Utils
 */
class Table {
    private $rows = [];

    public function addRow() {
        $this->rows[] = func_get_args();
        return $this;
    }

    public function get() {
        $columns = 0;
        $columnPadding = [];
        $returnData = [];

        foreach($this->rows as $row) {
            if (count($row) > $columns) {
                $columns = count($row);
            }

            foreach($row as $idx => $value) {
                if (!isset($columnPadding[$idx])) {
                    $columnPadding[$idx] = 0;
                }

                if (mb_strlen(Format::strip($value)) + 2 > $columnPadding[$idx]) {
                    $columnPadding[$idx] = mb_strlen(Format::strip($value)) + 2;
                }
            }
        }

        foreach($this->rows as $row) {
            $rowData = [];

            foreach($row as $idx => $value) {
                $paddingLength = $columnPadding[$idx] - mb_strlen(Format::strip($value));
                $padding = str_repeat(' ', $paddingLength);
                $rowData[$idx] = $value . $padding;
            }

            $returnData[] = implode('', $rowData);
        }

        return $returnData;
    }
}