<?php
namespace Ikx\Core\Utils;

class RandomWords {
    private static $words = [];

    private static function initWords() {
        $file = dirname(__FILE__) . '/words.txt';
        $contents = file_get_contents($file);
        $contents = str_replace("\r", "", $contents);
        self::$words = explode("\n", $contents);
    }

    public static function get($count = 1) {
        if(!count(self::$words)) { self::initWords(); }

        $w = [];
        for($i = 0; $i < $count; $i++) {
            $w[] = self::$words[array_rand(self::$words, 1)];
        }

        self::$words = [];

        return $w;
    }
}