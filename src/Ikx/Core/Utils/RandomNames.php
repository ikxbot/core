<?php
namespace Ikx\Core\Utils;

class RandomNames {
    private static $words = [];

    private static function initWords($type = 'male') {
        $file = dirname(__FILE__) . '/' . $type . '-names.txt';
        if (file_exists($file)) {
            $contents = file_get_contents($file);
            $contents = str_replace("\r", "", $contents);
            self::$words = explode("\n", $contents);
        }
    }

    public static function get($type = 'male', $count = 1) {
        if(!count(self::$words)) { self::initWords($type); }

        $w = [];
        for($i = 0; $i < $count; $i++) {
            $n = self::$words[array_rand(self::$words, 1)];
            $n = str_replace(' ', '-', $n);
            $w[] = $n;
        }

        self::$words = [];

        return $w;
    }
}