<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core;

use Ikx\Core\Console\RunCommand;

define('IKX_ROOT', realpath(dirname(__DIR__) . '/../../../../../'));

/**
 * Application bootstrapper interface
 * @package Ikx\Core
 */
class Bootstrap {
    /** @var Application Core application */
    private $application;

    /**
     * Create a new Ikx application
     * @return Application
     */
    public function createApplication() {
        if (!$this->application) {
            $this->application = new Application();
            $this->application->add(new RunCommand());
            /*$this->application->add(new AddCommand());
            $this->application->add(new DelCommand());
            $this->application->add(new GenerateCommand());
            $this->application->add(new SetCommand());*/
        }

        return $this->application;
    }
}