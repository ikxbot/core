<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Console;

use Ikx\Core\Application;
use Ikx\Core\Entity\Network;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Run command
 * @package Ikx\Core\Console
 */
class RunCommand extends Command {
    /**
     * @var string Command name
     */
    protected static $defaultName = 'run';

    public static $pids = [];
    public static $networks = [];

    /**
     * Commend configuration
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setDescription('Allow the bot to run');
    }

    /**
     * Command executor
     * @inheritdoc
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pidPath = sprintf('%s/Ikx.pid', IKX_ROOT);
        file_put_contents($pidPath, getmypid());

        self::$pids = [getmypid()];

        $networks = Application::config()->get('networks');
        for($i = 0; $i < count($networks); $i++) {
            $network = new Network($networks[$i]);
            self::$networks[] = $network;

            $pid = pcntl_fork();
            if ($pid > 0) {
                // We are the parent - save the PID
                self::$pids[] = $pid;
            } else {
                $network->connect();
            }

            sleep(3);
        }

        file_put_contents($pidPath, implode("\n", self::$pids));

        foreach(self::$pids as $pid) {
            pcntl_waitpid($pid, $status);
            $output->writeln("<error>Process {$pid} exited with status {$status}</error>");
        }

        unlink($pidPath);
    }
}