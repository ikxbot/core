<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Model;

use Noodlehaus\AbstractConfig;

/**
 * Class Config
 * @package Ikx\Core\Model
 * @deprecated
 */
final class Config extends AbstractConfig {
    /**
     * Fetches defaults from configuration
     * @deprecated
     * @return array|mixed|null
     */
    protected function getDefaults() {
        return $this->get('defaults', []);
    }
}