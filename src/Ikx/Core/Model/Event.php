<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Model;

use Ikx\Core\Event\AbstractEvent;

/**
 * Event model
 * @package Ikx\Core\Model
 */
class Event {
    const TYPE_JOIN = 'JOIN';
    const TYPE_LOGIN = '001';
    const TYPE_MODE = 'MODE';
    const TYPE_NICK = 'NICK';
    const TYPE_NOTICE = 'NOTICE';
    const TYPE_PING = 'PING';
    const TYPE_PRIVMSG = 'PRIVMSG';
    const TYPE_SERVERINFO = '005';
    const TYPE_WHOLINE = '352';

    /** @var array A list of all registered events */
    private static $eventList = [];

    /**
     * Register a new event
     * @param string $type
     * @param string $className
     */
    public static function register(
        string $type,
        string $className
    ) {
        if (!isset(self::$eventList[$type])) {
            self::$eventList[$type] = [];
        }

        self::$eventList[$type][] = $className;
        echo "Registered {$className} for {$type}\n";
    }

    /**
     * Fetch all events
     * @return array
     */
    public static function fetch() {
        return self::$eventList;
    }
}