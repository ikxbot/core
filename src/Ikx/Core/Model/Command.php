<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Model;

/**
 * Command model
 * @package Ikx\Core\Model
 */
class Command {
/** @var array A list of all registered commands */
    private static $commandList = [];

    /**
     * Register a new command
     * @param string $command           Command name
     * @param string $className         Command class
     * @param bool $inChannel           Toggle wether the command should be performed in channel or not
     * @param int $level                Minimum user level for this command
     */
    public static function register(
        string $command,
        string $className,
        $inChannel = true,
        $level = 0
    ) {
        $command = strtoupper($command);

        if (!isset(self::$commandList[$command])) {
            self::$commandList[$command] = [
                'class'     => $className,
                'inChannel' => $inChannel,
                'level'     => $level
            ];
        }
    }

    /**
     * Fetch all commands
     * @return array
     */
    public static function fetch() {
        ksort(self::$commandList);
        return self::$commandList;
    }
}