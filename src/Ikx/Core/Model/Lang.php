<?php
namespace Ikx\Core\Model;

class Lang {
    private static $strings = [];

    public static function addFromFile($path, $language = 'en_US') {
        if (!isset(self::$strings[$language])) {
            self::$strings[$language] = [];
        }

        if (file_exists($path)) {
            $fh = fopen($path, 'r');

            while($data = fgetcsv($fh)) {
                self::$strings[$language][$data[0]] = $data[1];
            }
        }
    }

    public static function addString($language, $source, $translation) {
        if (!isset(self::$strings[$language])) {
            self::$strings[$language] = [];
        }

        self::$strings[$language][$source] = $translation;
    }

    public static function getString($string, $language) {
        return self::$strings[$language][$string] ?? $string;
    }
}