<?php
namespace Ikx\Core\Model;

use Symfony\Component\Yaml\Yaml;

class Module {
    private static $modules = [];

    public static function register(
        $directory
    ) {
        $config = self::getConfig($directory);
        $config['enabled'] = true;                  // Disabling is impossible just yet
        $name   = $config['name'];

        if (!isset(self::$modules[$name])) {
            self::$modules[$name] = [];
        }

        $modCfg = &self::$modules[$name];
        $modCfg['config'] = $config;
        $modCfg['events'] = self::fetchEvents($directory);
        $modCfg['commands'] = self::fetchCommands($directory);

        self::loadTranslations($directory);
    }

    private static function loadTranslations($directory) {
        if (is_dir($directory . '/../i18n')) {
            $fetchDir = $directory . '/../i18n';
            $iterator = new \DirectoryIterator($fetchDir);

            foreach($iterator as $file) {
                if ($file->isFile() && $file->getExtension() == 'csv') {
                    $path = $file->getRealPath();
                    $filename = $file->getFilename();
                    $lang = explode('.', $filename)[0];

                    Lang::addFromFile($path, $lang);
                }
            }
        }
    }

    private static function getConfig($dir) {
        $filename = realpath($dir . '/../etc/module.yml');

        if (!$filename) {
            throw new \Exception("Can not register module in {$dir}: module.yml file not found");
        }

        return Yaml::parseFile($filename);
    }

    public static function getAll() {
        return self::$modules;
    }

    public static function get($name) {
        return self::$modules[$name] ?? null;
    }

    private static function fetchEvents($dir) {
        $filename = realpath($dir . '/../etc/events.yml');

        if ($filename) {
            return Yaml::parseFile($filename);
        }

        return [];
    }

    private static function fetchCommands($dir) {
        $filename = realpath($dir . '/../etc/commands.yml');

        if ($filename) {
            return Yaml::parseFile($filename);
        }

        return [];
    }

    public static function getAvailableEvents() {
        $commandList = [];

        foreach(self::$modules as $module) {
            if ($module['config']['enabled'] == true) {
                foreach($module['events'] as $command => $actions) {
                    foreach($actions as $cfg) {
                        if (!isset($commandList[$command])) {
                            $commandList[$command] = [];
                        }

                        $commandList[$command][] = $cfg;
                    }
                }
            }
        }

        return $commandList;
    }

    public static function getAvailableCommandsByModule($moduleName, $inChannel = true) {
        if (isset(self::$modules[$moduleName])) {
            $commandList = [];

            $module = self::$modules[$moduleName];
            if ($module['config']['enabled'] == true) {
                foreach($module['commands'] as $command => $actions) {
                    foreach($actions as $cfg) {
                        if ($cfg['inChannel'] == $inChannel) {
                            if (!isset($commandList[$command])) {
                                $commandList[$command] = [];
                            }

                            $cfg['module'] = $moduleName;
                            $commandList[$command][] = $cfg;
                        }
                    }
                }
            }

            return $commandList;
        }

        return null;
    }

    public static function getAvailableCommands($inChannel = true) {
        $commandList = [];

        foreach(self::$modules as $name => $module) {
            if ($module['config']['enabled'] == true) {
                foreach($module['commands'] as $command => $actions) {
                    foreach($actions as $cfg) {
                        if ($cfg['inChannel'] == $inChannel) {
                            if (!isset($commandList[$command])) {
                                $commandList[$command] = [];
                            }

                            $cfg['module'] = $name;
                            $commandList[$command][] = $cfg;
                        }
                    }
                }
            }
        }

        return $commandList;
    }
}