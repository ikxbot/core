<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Entity;

use Ikx\Core\Application;
use Ikx\Core\Utils\RandomNames;
use Ikx\Core\Utils\RandomWords;

/**
 * Network entity
 * @package Ikx\Core\Entity
 */
class Network {
    /** @var array Server array */
    private $servers;

    /** @var string Network identifier */
    private $identifier;

    /** @var string Bot's nickname */
    private $nickname;

    /** @var string Bot's alternative nicknames */
    private $altnick;

    /** @var string Bot's username/ident */
    private $username;

    /** @var string Bot's realname (gecos) */
    private $gecos;

    /** @var string Command prefix */
    private $prefix;

    /** @var string Bot's current nickname */
    private $currentNick;

    /** @var int Current server index */
    private $serverIndex = 0;

    /** @var array Channels on the network */
    private $channels = [];

    /** @var array Users on the network */
    private $users = [];

    /** @var Network Network instance */
    private static $instance;

    /** @var int My Process ID */
    private $pid;

    private $bindhost = '0.0.0.0';

    private $channelCfg = [];
    private $logchan = '#Ikx';
    private $config = [];
    private $server;

    /**
     * Network constructor.
     * @param array $config
     */
    public function __construct($config) {
        self::$instance = $this;
        $this->config = $config;

        $this->pid = getmypid();
        $this->servers = $config['address'];
        $this->identifier = $config['identifier'];
        $this->nickname = $config['me']['nickname'] ?? Application::config()->get('defaults.me.nickname');
        $this->altnick = $config['me']['altnick'] ?? Application::config()->get('defaults.me.altnick');
        $this->username = $config['me']['username'] ?? Application::config()->get('defaults.me.username');
        $this->gecos = $config['me']['gecos'] ?? Application::config()->get('defaults.me.gecos');
        $this->prefix = $config['options']['prefix'] ?? Application::config()->get('defaults.options.prefix');
        $this->channelCfg = $config['channels'] ?? [];
        $this->logchan = $config['options']['logchan'] ?? Application::config()->get('defaults.options.logchan');
        $this->bindhost = $config['options']['bindhost'] ?? Application::config()->get('defaults.options.bindhost');

        if (!$this->bindhost) {
            $this->bindhost = '0.0.0.0';
        }

        // $this->username = $this->generateRandomString(rand(6,12));
        $this->username = ucwords(implode(' ',RandomNames::get('male', 1)));
        $this->gecos = ucwords(implode(' ', RandomWords::get(rand(3, 8))));

        if (!is_array($this->altnick)) {
            $this->altnick = explode(' ', $this->altnick);
        }

        if (!is_array($this->nickname)) {
            $this->nickname = explode(' ', $this->nickname);
        }

        $timezone = $config['options']['timezone'] ?? Application::config()->get('defaults.options.timezone');
        $locale = $config['options']['locale'] ?? Application::config()->get('defaults.options.locale');
        Application::setLocale($locale);
        if ($timezone) {
            date_default_timezone_set($timezone);
        }

        if ($locale) {
            setlocale(LC_ALL, $locale);
        }
    }

    public static function generateRandomString($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWYXZ0123456789-';
        $string = '';
        for($i = 0; $i < $length; $i++) {
            $r = rand(0, mb_strlen($chars) - 1);
            $string .= $chars[$r];
        }
        return $string;
    }

    /**
     * Fetch the network instance
     * @return Network
     */
    public static function getInstance() : Network {
        return self::$instance;
    }

    /**
     * Connect to the next server in the list, or reconnect to the current server if there is only 1 available
     * @TODO Add wait time to reconnect
     */
    public function connect() : void {
        while (true) {
            $this->server = new Server($this->servers[$this->serverIndex]);
            $this->server->connect();

            $this->serverIndex++;
            if ($this->serverIndex >= count($this->servers)) {
                $this->serverIndex = 0;
            }
        }
    }

    /**
     * Fetch a variable from this class, since they are private to disallow overwriting
     * @param string $key
     * @return mixed|null
     */
    public function get($key) {
        if ($this->$key) {
            return $this->$key;
        }

        return null;
    }

    /**
     * Change the bot's current nick
     * @param string $nick
     */
    public function setCurrentNick($nick) {
        $this->currentNick = $nick;
    }

    /**
     * Create a new channel in memory
     * @param string $channelName
     * @return Channel
     */
    public function createChannel($channelName) {
        if (!isset($this->channels[$channelName])) {
            $this->channels[$channelName] = new Channel($channelName);
        }

        return $this->channels[$channelName];
    }

    /**
     * Get a channel from memory
     * @param $channelName
     * @return Channel|null
     */
    public function getChannel($channelName) {
        return $this->channels[$channelName] ?? null;
    }

    public function deleteChannel($channel) {
        if ($this->channels[$channel]) {
            /** @var User $user */
            foreach($this->users as $user) {
                if ($user->ison($channel)) {
                    $chan = $this->getChannel($channel);
                    $user->removeChannel($chan);
                }
            }

            unset($this->channels[$channel]);
        }
    }

    /**
     * Fetch a database instance
     * @return \MongoDB\Database
     */
    public function getDb() {
        $dbName = $this->getDbName();
        $db = (new \MongoDB\Client)->$dbName;
        return $db;
    }

    /**
     * Get the database name
     * @return string
     */
    public function getDbName() {
        $ident = $this->identifier;
        $ident = preg_replace('/[^a-zA-Z0-9\-_]/', '', $ident);
        return (string) $ident;
    }

    /**
     * @param string $nickname
     * @param string $username
     * @param string $address
     * @param string $gecos
     * @return mixed
     */
    public function createUser($nickname, $username, $address, $gecos) {
        if (!isset($this->users[$nickname])) {
            $this->users[$nickname] = new User($nickname, $username, $address);
            $this->users[$nickname]->setGecos($gecos);
        }

        return $this->users[$nickname];
    }

    public function getUser($nickname) {
        return $this->users[$nickname] ?? null;
    }

    public function changeUser($oldNick, $newNick) {
        /** @var User $user */
        $user = $this->users[$oldNick];
        if ($user) {
            $user->setNickname($newNick);

            $this->users[$newNick] = $this->users[$oldNick];
            unset($this->users[$oldNick]);
        }
    }

    public function deleteUser($user) {
        unset($this->users[$user]);
    }

    public function setPrefix($prefix) {
        $this->prefix = $prefix;
        return $this;
    }
}