<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Entity;

/**
 * Channel entity
 * @package Ikx\Core\Entity
 */
class Channel extends AbstractEntity {
    /** @var string Channel name */
    private $name = '';
    /** @var string Channel topic */
    private $topic = '';
    /** @var int Channel topic set at timestamp */
    private $topicSetAt = 0;
    /** @var string Channel topic set by nick */
    private $topicSetBy = '';
    /** @var int Channel created at timestamp */
    private $createdAt = 0;
    /** @var string Channel modes */
    private $modes = '+';
    /** @var array Users on channel */
    private $users = [];

    /**
     * Channel constructor.
     * @param string $name Channel name
     */
    public function __construct($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    /**
     * Set the topic
     * @param string $topic
     * @return $this
     */
    public function setTopic($topic) {
        $this->topic = $topic;
        return $this;
    }

    /**
     * Get the topic
     * @return string
     */
    public function getTopic() {
        return $this->topic;
    }

    /**
     * Set the timestamp the topic was set
     * @param string $value
     * @return $this
     */
    public function setTopicSetAt($value) {
        $this->topicSetAt = $value;
        return $this;
    }

    /**
     * Get the topic set timestamp
     * @return int
     */
    public function getTopicSetAt() {
        return $this->topicSetAt;
    }

    /**
     * Set the nick the topic was set by
     * @param string $nick
     * @return $this
     */
    public function setTopicSetBy($nick) {
        $this->topicSetBy = $nick;
        return $this;
    }

    /**
     * Get the nick the topic was set by
     * @return string
     */
    public function getTopicSetBy() {
        return $this->topicSetBy;
    }

    /**
     * Set the channel modes
     * @param string $modes
     * @return $this
     */
    public function setModes($modes) {
        $this->modes = $modes;
        return $this;
    }

    /**
     * Get the channel modes
     * @return string
     */
    public function getModes() {
        return $this->modes;
    }

    /**
     * Set the timestamp the channel was created
     * @param string $value
     * @return $this
     */
    public function setCreatedAt($value) {
        $this->createdAt = $value;
        return $this;
    }

    /**
     * Get the channel creation timestamp
     * @return int
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }
}