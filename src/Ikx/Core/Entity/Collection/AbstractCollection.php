<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Entity\Collection;

/**
 * Class AbstractCollection
 * @package Ikx\Core\Entity\Collection
 */
abstract class AbstractCollection {
    /** @var array Collection items */
    private static $items = [];

    /**
     * Find an items by the value of a field
     *
     * @param string $field
     * @param AbstractEntity|null $value
     * @return mixed|null
     */
    public function find($field, $value) {
        foreach(self::$items as $item) {
            if (
                array_key_exists($field, $item) &&
                $item->getData($field) == $value
            ) {
                return $item;
            }
        }

        return null;
    }
}