<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Entity;

use Ikx\Core\Application;
use Ikx\Core\Event\AbstractEvent;
use Ikx\Core\Model\Event;
use Ikx\Core\Model\Module;

/**
 * Server entity
 * @package Ikx\Core\Entity
 */
class Server {
    /** @var string IP to connect to */
    private $ip = '';

    /** @var int Port to connect on */
    private $port = 6667;

    /** @var bool Toggle SSL */
    public $ssl = false;

    /** @var resource Connection socket */
    private $socket;

    /** @var Server Server instance */
    private static $instance;

    /** @var int Maximum number of channels */
    private $maxchannels = 0;

    /** @var array Channel limit per type */
    private $chanlimit = ['#' => 0];

    /** @var int Maximum nick length */
    private $maxnicklen = 0;

    /** @var int Nick lnegth */
    private $nicklen = 0;

    /** @var int Channel length */
    private $channellen = 0;

    /** @var int Topic length */
    private $topiclen = 0;

    /** @var int Kick length */
    private $kicklen = 0;

    /** @var int Away length */
    private $awaylen = 0;

    /** @var int Maximum number of targets */
    private $maxtargets = 0;

    /** @var array Channel types */
    private $chantypes = ['#'];

    /** @var string Channel nickname prefixes */
    private $prefixes = '';

    /** @var int Queue process ID */
    private $queuePid;
    /** @var string Password */
    private $pass = '';

    /**
     * Server constructor.
     * @param string $connectString
     */
    public function __construct($connectString) {
        self::$instance = $this;

        $parts = explode(' ', $connectString);
        if (count($parts) > 0) {
            $this->pass = $parts[1];
        }
        $connectString = $parts[0];

        $parts = explode(':', $connectString);
        $this->ip = $parts[0];

        if (substr($parts[1], 0, 1) == '+') {
            $this->ssl = true;
            $this->port = (int) substr($parts[1], 1);
        } else {
            $this->port = (int) $parts[1];
        }
    }

    /**
     * Fetch a Server instance
     * @return Server
     */
    public static function getInstance() : Server {
        return self::$instance;
    }

    /**
     * Connect to the server and sign on
     */
    public function connect() : void {
        // 1. Prepare the USER string
        $username = Network::getInstance()->get('username');
        $gecos = Network::getInstance()->get('gecos');
        $userString = sprintf('USER %s * 0 :%s', $username, $gecos);

        // 2. Prepare the NICK string
        // $nick = Network::getInstance()->get('nickname')[0];
        $nick = Network::getInstance()->get('nickname')[array_rand(Network::getInstance()->get('nickname'), 1)];
        $nickString = sprintf('NICK %s', $nick);
        Network::getInstance()->setCurrentNick($nick);

        // 3. Prepare the socket instance
        $this->createSocket();
        if (Network::getInstance()->getDb()->queue->countDocuments()) {
            Network::getInstance()->getDb()->queue->drop();
        }

        // 4. Write connect data to the socket
        if ($this->pass != '') {
            $this->_write('PASS :' . $this->pass);
        }
        $this->_write('CAP LS');
        $this->_write($userString);
        $this->_write($nickString);
        $this->startQueue();

        // 5. Read data from socket
        $this->read();
    }
    /**
     * Read incoming data from the server
     */
    private function read() {
        while ($this->socket && !feof($this->socket)) {
            $data = str_replace(chr(160), ' ', trim(fgets($this->socket, 512)));
            if (mb_strlen($data) > 5) {
                echo "<< [" . Network::getInstance()->get('identifier') . "/" . getmypid() . "/" . date('H:i:s') . "] " . $data . PHP_EOL;
            } else {
                $this->_write('PING :' . Network::generateRandomString(20));
            }

            $ex = explode(' ', $data);

            if (substr($ex[0], 0, 1) != ':') {
                $eventName = $ex[0];
            } else {
                $eventName = $ex[1];
            }

            if (isset($ex[1]) && $ex[1] == 'CAP') {
                // No event for this, capability detection is a core feature which should always be available
                if ($ex[3] == 'LS') {
                    // Capabilities are being listed
                    $this->_write('CAP REQ :chghost away-notify userhost-in-names extended-join');
                }
                if ($ex[3] == 'ACK' || $ex[3] == 'NAK') {
                    $this->_write('CAP END');
                }
            }

            $events = Module::getAvailableEvents();
            foreach($events as $event => $classes) {
                if ($eventName == $event) {
                    foreach ($classes as $className) {
                        $ignoreCollection = Network::getInstance()->getDb()->ignore;

                        $nick = '';
                        $address = '';

                        if (substr($ex[0], 0 ,1) == ':') {
                            $ex1 = explode('@', $ex[0]);
                            if (count($ex1) > 1) {
                                $address = $ex1[1];
                                $ex2 = explode('!', $ex1[0]);
                                $nick = substr($ex2[0], 1);
                            }
                        }

                        /** @var User $user */
                        $user = Network::getInstance()->getUser($nick);
                        if ($user) {
                            if ($user->getAddress() != $address) {
                                $user->setAddress($address);
                                $this->log('Updated address for ' . $nick);
                            }
                        }

                        $resultNick = $ignoreCollection->findOne(['nickname' => $nick]);
                        $resultAddr = $ignoreCollection->findOne(['address' => $address]);

                        if ($ex[1] != 'PRIVMSG' && $ex[1] != 'NOTICE') {
                            $resultAddr = $resultNick = null;
                        }

                        if (!$resultNick && !$resultAddr) {
                            /** @var AbstractEvent $class */
                            $class = new $className($this, Network::getInstance(), $ex);
                            $class->execute();
                        }
                    }
                }
            }
/*
            $events = Event::fetch();
            if (isset($events[$eventName])) {
                foreach($events[$eventName] as $className) {
                    /** @var AbstractEvent $class *
                    $class = new $className($this, Network::getInstance(), $ex);
                    $class->execute();
                }
            }*/

            $this->startQueue();
        }

        pcntl_waitpid($this->queuePid, $status);
        unset($this->queuePid);
    }

    public function startQueue() {
        if (Application::config()->get('options.queue.enabled')) {
            if (!$this->queuePid) {
                $pid = pcntl_fork();
                if ($pid > 0) {
                    $this->queuePid = $pid;
                } else if (!$pid) {
                    $this->runQueue();
                }
            }
        }
    }

    public function runQueue() {
        while (true) {
            $db = Network::getInstance()->getDb();

            if ($db->queue->countDocuments()) {
                $max = 8;
                if ($db->queue->countDocuments() < 8) {
                    $max = $db->queue->countDocuments();
                }

                for ($i = 0; $i < $max; $i++) {
                    $result = $db->queue->findOne();

                    $db->queue->findOneAndDelete(['_id' => $result['_id']]);
                    $this->_write($result['data']);
                }

                // Sleep for a while before moving to the next line in the queue
                usleep($db->queue->count() * 40000);
            } else {
                usleep(100000);
            }
        }
    }

    /**
     * Write raw data to the server
     * @param string $data
     */
    public function write($data) {
        if (Application::config()->get('options.queue.enabled')) {
            $db = Network::getInstance()->getDb();
            $db->queue->insertOne([
                'data' => $data
            ]);
        } else {
            $this->_write($data);
        }
    }

    private function _write($data) {
        if ($this->socket && !feof($this->socket)) {
            fputs($this->socket, $data . "\n");
            echo ">> [" . Network::getInstance()->get('identifier') . "/" . getmypid() . "/" . date('H:i:s') . "] " . $data . PHP_EOL;
        }
    }
    /**
     * Creates a new socket
     * @return bool|resource
     */
    private function createSocket() {
        // 1. Create the connection string
        $proto = $this->ssl ? 'ssl' : 'tcp';
        $connectString =  sprintf('%s://%s:%s', $proto, $this->ip, $this->port);

        // 2. Create the socket context
        $options = [
            'ssl'       => [
                'verify_peer'       => false,
                'verify_peer_name'  => false
            ],
            'socket'    => [
                'bindto'            => Network::getInstance()->get('bindhost') . ':0',
                'tcp_nodelay'       => 1
            ]
        ];

        $localCertPath = IKX_ROOT . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'ssl' . DIRECTORY_SEPARATOR . 'ikx.pem';
        if (file_exists($localCertPath)) {
            $options['ssl']['local_cert'] = $localCertPath;
        }

        $context = stream_context_create($options);


        // 3. Create and return the socket
        $this->socket = stream_socket_client(
            $connectString,
            $errno,
            $errstr,
            5,
            STREAM_CLIENT_CONNECT,
            $context
        );

        if (!$this->socket) {
            echo "ERROR - {$errstr}";
            sleep(10);
        }

        stream_set_timeout($this->socket, 30);

        return $this->socket;
    }


    /**
     * Set the maximum number of channel a user is allowed to be on
     * @param int $value
     * @return $this
     */
    public function setMaxChannels($value) {
        $this->maxchannels = $value;
        return $this;
    }

    /**
     * Get the maximum number of channel a user is allowed to be on
     * @return int
     */
    public function getMaxChannels() {
        return $this->maxchannels;
    }

    /**
     * Set the channel limit per channel type
     * @param int $value
     * @return $this
     */
    public function setChanLimit($value) {
        $chanLimits = [];
        $values = explode(',', $value);
        foreach($values as $v) {
            $v = explode(':', $v);
            $chanLimits[] = [
                $v[0] => $v[1]
            ];
        }

        $this->chanlimit = $chanLimits;
        return $this;
    }

    /**
     * Get the channel limit per channel type
     * @return array
     */
    public function getChanLimit() {
        return $this->chanlimit;
    }

    /**
     * Set the maximum nick length
     * @param int $value
     * @return $this
     */
    public function setMaxNicklen($value) {
        $this->maxnicklen = $value;
        return $this;
    }

    /**
     * Get the maximum nick length
     * @return int
     */
    public function getMaxNicklen() {
        return $this->maxnicklen;
    }

    /**
     * Set the nick length
     * @param int $value
     * @return $this
     */
    public function setNicklen($value) {
        $this->nicklen = $value;
        return $this;
    }

    /**
     * Get the nick length
     * @return int
     */
    public function getNicklen() {
        return $this->nicklen;
    }

    /**
     * Set the channel name length
     * @param int $value
     * @return $this
     */
    public function setChannellen($value) {
        $this->channellen = $value;
        return $this;
    }

    /**
     * Get the channel name length
     * @return int
     */
    public function getChannellen() {
        return $this->channellen;
    }

    /**
     * Set the maximum topic length
     * @param int $value
     * @return $this
     */
    public function setTopiclen($value) {
        $this->topiclen = $value;
        return $this;
    }

    /**
     * Get the maximum topic length
     * @return int
     */
    public function getTopiclen() {
        return $this->topiclen;
    }

    /**
     * Get the maximum kick length
     * @param int $value
     * @return $this
     */
    public function setKicklen($value) {
        $this->kicklen = $value;
        return $this;
    }

    /**
     * Set the maximum kick length
     * @return int
     */
    public function getKicklen() {
        return $this->kicklen;
    }

    /**
     * Set the maximum away length
     * @param int $value
     * @return $this
     */
    public function setAwaylen($value) {
        $this->awaylen = $value;
        return $this;
    }

    /**
     * Get the maximum away length
     * @return int
     */
    public function getAwaylen() {
        return $this->awaylen;
    }

    /**
     * Set the maximum number of targets
     * @param int $value
     * @return $this
     */
    public function setMaxtargets($value) {
        $this->maxtargets = $value;
        return $this;
    }

    /**
     * Get the maximum number of targets
     * @return int
     */
    public function getMaxtargets() {
        return $this->maxtargets;
    }

    /**
     * Set the allowed channel types
     * @param string $value
     * @return $this
     */
    public function setChantypes($value) {
        $chantypes = [];
        for($i = 0; $i < strlen($value); $i++) {
            $chantypes[] = substr($value, $i, 1);
        }
        $this->chantypes = $chantypes;

        return $this;
    }

    /**
     * Get allowed channel types
     * @return array
     */
    public function getChantypes() {
        return $this->chantypes;
    }

    /**
     * Set user in channel prefixes
     * @TODO fix this           --- PREFIX=(yqaohv)!~&@%+
     * @param $value
     * @return $this
     */
    public function setPrefix($value) {
        return $this;
    }

    /**
     * Get user in channel prefixes
     * @return string
     */
    public function getPrefix() {
        return $this->prefixes;
    }

    public function log($data) {
        $logchan = Network::getInstance()->get('logchan');
        if ($logchan != "") {
            $this->_write(sprintf("PRIVMSG %s :%s", $logchan, $data));
        }
    }
}