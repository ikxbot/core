<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Entity;

use Ikx\Core\Entity\Collection\AbstractCollection;

/**
 * Abstract entity
 * @package Ikx\Core\Entity
 */
abstract class AbstractEntity {
    /** @var int ID field */
    protected $idField;
    /** @var AbstractCollection Collection holder */
    protected $collection;
    /** @var string Collection class name */
    protected $collectionClass;

    /**
     * Return an entity by it's identifier
     *
     * @param mixed $identifier
     * @return mixed
     */
    public function get($identifier) {
        return $this->getCollection()->find($this->idField, $identifier);
    }

    /**
     * Return the collection for this entity
     * @return AbstractCollection
     */
    public function getCollection() {
        if (!$this->collection) {
            $this->collection = new $this->collectionClass;
        }

        return $this->collection;
    }
}