<?php
namespace Ikx\Core\Entity;

class User {
    /** @var string User nickname */
    private $nickname;
    /** @var string User username (ident) */
    private $username;
    /** @var string User address */
    private $address;
    /** @var string User gecos (realname) */
    private $gecos;
    /** @var string Login username */
    private $loginName = 'Guest';
    /** @var int User level */
    private $level = 0;
    /** @var array Channels the user is on */
    private $channels = [];
    /** @var array Channel levels for this user */
    private $channelLevels = [];

    /** @var bool Define wether the user is using a secure connection or not */
    private $isSecure = false;

    private $version = '';

    const LEVEL_GUEST = 0;
    const LEVEL_VOICE = 10;
    const LEVEL_LOGGEDIN = 11;
    const LEVEL_HALFOP = 20;
    const LEVEL_OP = 30;
    const LEVEL_ADMIN = 40;
    const LEVEL_OWNER = 50;
    const LEVEL_OPER = 60;
    const LEVEL_BOTOWNER = 1000;

    /**
     * User constructor.
     * @param string $nickname
     * @param string $username
     * @param string $address
     */
    public function __construct($nickname, $username, $address) {
        $this->nickname = $nickname;
        $this->username = $username;
        $this->address = $address;
    }

    public function setNickname($nickname) {
        $this->nickname = $nickname;
        return $this;
    }

    public function getNickname() {
        return $this->nickname;
    }

    public function setUsername($username) {
        $this->username = $username;
        return $this;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setAddress($address) {
        $this->address = $address;
        return $this;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setGecos($gecos) {
        $this->gecos = $gecos;
        return $this;
    }

    public function getGecos() {
        return $this->gecos;
    }

    public function setLevel($level) {
        $this->level = $level;
        return $this;
    }

    public function getLevel() {
        return $this->level;
    }

    public function setLoginName($name) {
        $this->loginName = $name;
        return $this;
    }

    public function getLoginName() {
        return $this->loginName;
    }

    public function addChannel(Channel &$channel) {
        $this->channels[$channel->getName()] = $channel;
        return $this;
    }

    public function removeChannel(Channel &$channel) {
        unset($this->channels[$channel->getName()]);
    }

    public function getChannelNames() {
        ksort($this->channels);

        return array_keys($this->channels);
    }

    public function getChannelNamesWithLevel() {
        ksort($this->channels);

        $channels = array_keys($this->channels);
        $data = [];

        foreach($channels as $channel) {
            $level = $this->getChannelLevel($channel);
            $data[] = $level . ':' . $channel;
        }

        return $data;
    }

    public function ison($channel) {
        return array_key_exists($channel, $this->channels);
    }

    public function setChannelLevel($channel, $level) {
        $this->channelLevels[$channel] = $level;
    }

    public function getChannelLevel($channel) {
        return $this->channelLevels[$channel] ?? null;
    }

    public function getVersion() {
        return $this->version;
    }

    public function setVersion($version) {
        $this->version = $version;
        return $this;
    }

    public function setIsSecure($secure = true) {
        $this->isSecure = $secure;
        return $this;
    }

    public function getIsSecure() {
        return $this->isSecure;
    }
}