<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\Server;

/**
 * Abstract event
 * @package Ikx\Core\Event
 */
abstract class AbstractEvent {
    /** @var Server Server entity */
    protected $server;
    /** @var Network Network entity */
    protected $network;
    /** @var array Incoming data parts */
    protected $parts;

    /** @var string Event nickname */
    protected $nick = '';
    /** @var string Event username */
    protected $username = '';
    /** @var string Event address */
    protected $address = '';
    /** @var string Event target */
    protected $target = '';
    /** @var string Event channel */
    protected $channel = '';

    /**
     * AbstractEvent constructor.
     * @param Server $server            Current server instance
     * @param Network $network          Current network instance
     * @param $parts                    Incoming data parts
     */
    public function __construct(
        Server $server,
        Network $network,
        $parts
    ) {
        $this->server = $server;
        $this->network = $network;
        $this->parts = $parts;

        if (substr($parts[0], 0 ,1) == ':') {
            preg_match("/:(.+)!(.+)@(.+)/", $parts[0], $matches);
            if (count($matches) > 1) {
                $this->nick = $matches[1];
                $this->username = $matches[2];
                $this->address = $matches[3];
            }
        }

        if (count($parts) >= 3) {
            if (substr($parts[2], 0, 1) == ':') {
                $this->target = substr($parts[2], 1);
            } else {
                $this->target = $parts[2];
            }

            if(in_array(substr($this->target, 0 ,1), $this->server->getChantypes())) {
                $this->channel = $this->target;
            }
        }
    }
}