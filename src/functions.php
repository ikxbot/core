<?php
function __() {
    $args = func_get_args();

    $translation = \Ikx\Core\Model\Lang::getString($args[0], \Ikx\Core\Application::getLocale());

    array_shift($args);

    array_unshift($args, $translation);

    $data = call_user_func_array('sprintf', $args);

    return $data;
}